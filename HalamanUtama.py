import tkinter as tk
from tkinter import messagebox

import os
import sqlite3

from PembelianTiket import *
from TiketSaya import *
from TopUp import *
from Panduan import *

# pembuatan kelas frame HalamanUtama

class HalamanUtama(tk.Frame):

    # fungsi init atau constructor
    def __init__(self, master):
        tk.Frame.__init__(self, master)

        # background image
        self.canvas = tk.Canvas(self, width=640, height=480)
        self.canvas.pack()
        self.bg = self.canvas.create_image(
            0, 0, anchor=tk.NW, image=master.img_bg)

        self.font = "Helvetica"
        if os.name == 'nt':
            self.font = "Sans Serif"

        # membuat button pembelian tiket
        self.btn_beli_tiket = tk.Button(self, text="Beli Tiket", font=(self.font, 8), width=80,
                                        image=master.img_beli_tiket, compound="top", bg="white",
                                        command=lambda: master.switch_frame(PembelianTiket))
        self.add(self.btn_beli_tiket, 280, 140)

        # membuat button untuk menampilkan tiket yang telah dibeli
        self.btn_tiket_saya = tk.Button(self, text="Tiket Saya", font=(self.font, 8), width=80,
                                        image=master.img_tiket_saya, compound="top", bg="white",
                                        command=self.cek_tiket)
        self.add(self.btn_tiket_saya, 395, 140)

        # membuat button untuk menampilkan jadwal kereta api dalam 1 bulan
        self.btn_topup = tk.Button(self, text="Top Up", font=(self.font, 8), width=80,
                                    image=master.img_jadwal, compound="top", bg="white",
                                    command=lambda: master.switch_frame(TopUp))
        self.add(self.btn_topup, 510, 140)

        # membuat button panduan aplikasi
        self.btn_panduan = tk.Button(self, text="Panduan", font=(self.font, 8), width=80,
                                     image=master.img_panduan, compound="top", bg="white",
                                     command=lambda: master.switch_frame(Panduan))
        self.add(self.btn_panduan, 510, 210)

    def add(self, widget, x, y):
        canvas_window = self.canvas.create_window(
            x, y, anchor=tk.NW, window=widget)

    def cek_tiket(self):
        conn = sqlite3.connect("db/Tiket.db")

        cursor = conn.execute("SELECT id FROM TiketSaya")

        tiket = False

        for row in cursor:
            tiket = True
            break

        print(tiket)

        if tiket:
            self.master.switch_frame(TiketSaya)
        else:
            messagebox.showwarning("Gagal", "Anda belum memiliki tiket, silahkan beli terlebih dahulu pada menu Beli Tiket")

        conn.close()
