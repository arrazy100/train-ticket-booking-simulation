import tkinter as tk
from tkinter import ttk
from tkinter import messagebox

from tkcalendar import Calendar, DateEntry

import os
import re
import sqlite3
import math
import random

# pembuatan kelas entry hanya angka
class EntryDigit(tk.Entry):
    def __init__(self, master=None, **kwargs):
        self.var = tk.StringVar()
        tk.Entry.__init__(self, master, textvariable=self.var, **kwargs)
        self.old_value = ''
        self.var.trace('w', self.check)
        self.get, self.set = self.var.get, self.var.set

    def check(self, *args):
        if len(self.get()) > 17:
            self.set(self.get()[:-1])
        elif self.get().isdigit() or self.get() == "":
            self.old_value = self.get()
        else:
            self.set(self.old_value)

# pembuatan kelas PembelianTiket
class PembelianTiket(tk.Frame):

    # fungsi init atau constructor
    def __init__(self, master):
        tk.Frame.__init__(self, master)

        # font
        self.font = 'Helvetica Neue Light'
        if os.name == 'nt':
            self.font = 'Sans Serif'

        # pages
        self.cur_pages = 0

        # background image
        self.canvas = tk.Canvas(self, width=640, height=480)
        self.canvas.pack()

        # membuat canvas untuk list tiket
        self.frame_canvas_tiket = []
        self.old_list = []
        self.old_btn = []
        self.harga = []

        # ukuran font
        self.u_font = 8

        # perulangan untuk membuat canvas pada setiap tiket
        for i in range(0, len(master.nama_kereta)):

            canvas = tk.Canvas(
                self, width=300, height=80, bg="white")

            awal_tujuan = master.awal[i] + " to " + master.tujuan[i]

            if master._kelas[i] == "Ekonomi":
                if master.jarak[i] <= 100:
                    harga_tiket = 8000
                else:
                    harga_tiket = 80 * master.jarak[i]

            elif master._kelas[i] == "Eksekutif":
                if master.jarak[i] <= 100:
                    harga_tiket = 10000
                else:
                    harga_tiket = 150 * master.jarak[i]

            self.harga.append(harga_tiket)

            # membuat label pada tiket
            lbl_tiket = tk.Label(
                canvas, text=master.nama_kereta[i], font=(self.font, self.u_font), bg="white")
            lbl_harga = tk.Label(
                canvas, text="Rp. " + str(harga_tiket) + "/pax", font=(self.font, 10, "bold"), bg="white")
            lbl_kelas = tk.Label(
                canvas, text=master._kelas[i], font=(self.font, self.u_font), bg="white")
            lbl_tujuan = tk.Label(
                canvas, text=awal_tujuan, font=(self.font, self.u_font), bg="white")
            lbl_berangkattiba = tk.Label(
                canvas, text= self.master.berangkat[i] + " until " + self.master.tiba[i], font=(self.font, self.u_font), bg='white')

            # membuat id label dan menambahkan label ke dalam canvas
            lbl_tiket_w = self.add_in_frame(canvas, lbl_tiket, 5, 5)
            lbl_harga_w = self.add_in_frame(canvas, lbl_harga, 180, 10)
            lbl_kelas_w = self.add_in_frame(canvas, lbl_kelas, 5, 20)
            lbl_tujuan_w = self.add_in_frame(canvas, lbl_tujuan, 5, 60)
            lbl_berangkattiba_w = self.add_in_frame(canvas, lbl_berangkattiba, 5, 40)

            # menambahkan event mouse klik, mouse memasuki area, dan mouse keluar dari area canvas
            canvas.bind("<Button-1>", lambda event, i=i: self.klik(i))
            canvas.bind("<Enter>", lambda event, i=i: self.fokusin(i))
            canvas.bind("<Leave>", lambda event, i=i: self.fokusout(i))

            # sama dengan diatas, bedanya disini ditambahkan bind untuk semua elemen anak pada canvas
            lbl_tiket.bind("<Button-1>", lambda event, i=i: self.klik(i))
            lbl_harga.bind("<Button-1>", lambda event, i=i: self.klik(i))
            lbl_kelas.bind("<Button-1>", lambda event, i=i: self.klik(i))
            lbl_tujuan.bind("<Button-1>", lambda event, i=i: self.klik(i))
            lbl_berangkattiba.bind("<Button-1>", lambda event, i=i: self.klik(i))

            # menambahkan canvas baru ke dalam list
            self.frame_canvas_tiket.append(canvas)

        # dropdown list stasiun awal
        self.str_a_init = "Pilih Stasiun Keberangkatan"
        self.s_awal_combobox = ttk.Combobox(
            self, width=25, state='readonly', values=master.s_awal)
        self.s_awal_combobox.set(self.str_a_init)
        self.s_awal_combobox.pack()
        self.s_awal_combobox.bind(
            "<Button-1>", lambda event: self.resetBox(self.s_tujuan_combobox))
        self.add(self.s_awal_combobox, 50, 100)

        # dropdown list stasiun tujuan
        self.str_t_init = "Pilih Stasiun Tujuan"
        self.s_tujuan_combobox = ttk.Combobox(self, width=25, state='disabled')
        self.s_tujuan_combobox.set(self.str_t_init)
        self.s_tujuan_combobox.pack()
        self.add(self.s_tujuan_combobox, 50, 125)
        self.s_tujuan_combobox.bind(
            "<Button-1>", lambda event: self.stateBox(self.s_tujuan_combobox))

        # date entry
        self.date_entry = DateEntry(self, width=25, background='darkblue', foreground='white')
        self.add(self.date_entry, 50, 160)

        # button pencarian
        self.btn_cari_kereta = tk.Button(self, text="Cari", font=(
            self.font, self.u_font), command=self.cari_kereta)
        self.add(self.btn_cari_kereta, 50, 195)

        self.btn_home = tk.Button(self, text="Kembali", font=(self.font, self.u_font),
                                  command=lambda: master.switch_frame(master.HalamanUtama))
        self.add(self.btn_home, 10, 430)

        self.btn_beli = tk.Button(self, text="Beli", font=(
                                    self.font, 8), command=lambda: master.switch_frame(BayarTiket))
        self.btn_obj = self.add(self.btn_beli, 490, 430)
        self.canvas.itemconfigure(self.btn_obj, state='hidden')

        # background image
        self.bg = self.canvas.create_image(
            0, 0, anchor=tk.NW, image=self.master.bg_beli_tiket)

    # fungsi untuk menambahkan widget ke dalam canvas
    def add(self, widget, x, y):
        canvas_window = self.canvas.create_window(
            x, y, anchor=tk.NW, window=widget)
        return canvas_window

    # fungsi untuk menambahkan widget ke dalam canvas berdasarkan parameter frame
    def add_in_frame(self, frame, widget, x, y):
        canvas_window = frame.create_window(x, y, anchor=tk.NW, window=widget)
        return canvas_window

    # fungsi untuk membuat button halaman
    def generate_button_pages(self, lst, number_of_pages):
        for btn in self.old_btn:
            btn.pack()
            btn.pack_forget()

        x = 250
        for i in range(number_of_pages):
            btn = tk.Button(self, text=str(i+1), command=lambda i=i: self.klik_button_halaman(lst, i))
            self.add(btn, 290 + (35 * i), 370)
            self.old_btn.append(btn)

    def get_width(self, widget):
        bound = self.canvas.bbox(widget)
        return bound[2] - bound[0]

    def klik_button_halaman(self, lst, id):
        for l in self.old_list:
            l.pack()
            l.pack_forget()

        self.old_list = lst
        self.cur_pages = 0

        self.show_list(lst, id + 1, 3)

    # fungsi cari kereta
    def cari_kereta(self):
        # implementasi prolog logika
        query = "validasi_input({}, {}, {}, {}, X)".format(self.s_awal_combobox.get().replace(" ", ""),
                                                            self.str_a_init.replace(" ", ""),
                                                            self.s_tujuan_combobox.get().strip().replace(" ", ""),
                                                            self.str_t_init.strip().replace(" ", ""))
        stasiun_awal_dan_tujuan_kosong = self.master.eksekusi_prolog(query, "X")

        if stasiun_awal_dan_tujuan_kosong:
            messagebox.showwarning("Gagal", "Masukkan stasiun awal dan tujuan")
            return

        number_of_pages = 3
        lst = [i for i, x in enumerate(self.master.awal) if x == self.s_awal_combobox.get()
               and self.master.tujuan[i] == self.s_tujuan_combobox.get()]
        kereta = []
        for i in lst:
            kereta.append(self.frame_canvas_tiket[i])

        for l in self.old_list:
            l.pack()
            l.pack_forget()

        self.old_list = kereta
        self.cur_pages = 0

        self.generate_button_pages(kereta, self.count_pages(kereta, number_of_pages))
        self.show_list(kereta, 1, number_of_pages)

    # fungsi untuk enable combobox
    def stateBox(self, box):
        if (self.s_awal_combobox.get() != self.str_a_init):
            stasiun = self.master.a_t[self.s_awal_combobox.get()]
            stasiun = list(dict.fromkeys(stasiun))
            box.config(state='readonly', values=stasiun)
        else:
            messagebox.showwarning(
                "Warning", "Pilih stasiun keberangkatan terlebih dahulu")
            box.config(state='disabled')

    # fungsi untuk reset combobox tujuan
    def resetBox(self, box):
        box.set(self.str_t_init)

    # fungsi callback jika salah satu daftar tiket diklik
    def klik(self, id):
        self.master.b_kereta = self.master.nama_kereta[id]
        self.master.stasiun_awal = self.master.awal[id]
        self.master.stasiun_tujuan = self.master.tujuan[id]
        self.master.kelas = self.master._kelas[id]
        self.master.harga = self.harga[id]
        self.master.berangkattiba = self.master.berangkat[id] + " until " + self.master.tiba[id]
        self.master.tanggal = self.date_entry.get()
        self.canvas.itemconfigure(self.btn_obj, state='normal')

    # fungsi callback jika mouse memasuki area list tiket
    def fokusin(self, id):
        self.frame_canvas_tiket[id].config(highlightbackground='gray')

    # fungsi callback jika mouse keluar dari area list tiket
    def fokusout(self, id):
        self.frame_canvas_tiket[id].config(highlightbackground='white')

    def count_pages(self, lst, number_per_pages):

        page_item = [lst[i:i+number_per_pages]
                     for i in range(0, len(lst), number_per_pages)]
        c = 0

        for i in range(len(page_item)):
            c += 1

        return c

    # fungsi untuk menampilkan daftar tiket berdasarkan halaman
    def show_list(self, lst, pages, number_per_pages):

        if self.cur_pages == pages:
            return

        # memecah list menjadi list of list dengan setiap element list of list berdasarkan jumlah halaman yang telah ditentukan
        # implementasi prolog list of list

        angka_list = []
        for i in range(len(lst)):
            angka_list.append(i)

        q = "page_item({}, {}, X)".format(angka_list, number_per_pages)
        page_index = self.master.eksekusi_prolog(q, "X")

        # perulangan untuk memunculkan semua daftar tiket
        for i in range(len(page_index[pages - 1])):
            cur = lst[page_index[pages - 1][i]]
            y = 100 + (90 * i)
            cur.pack()
            self.add(cur, 290, y)

        self.cur_pages = pages

# pembuatan kelas BayarTiket
class BayarTiket(tk.Frame):

    # fungsi init atau constructor
    def __init__(self, master):
        tk.Frame.__init__(self, master)

        # font
        self.font = 'Helvetica Neue Light'

        # background image
        self.canvas = tk.Canvas(self, width=640, height=480)
        self.canvas.pack()

        # label tiket bagian atas
        self.str_nama = "Perjalanan Pergi -- " + self.master.b_kereta + " -- " + self.master.tanggal
        self.lbl_nama = tk.Label(self, text=self.str_nama, bg='white')
        self.str_stasiun = master.stasiun_awal + " > " + master.stasiun_tujuan
        self.lbl_stasiun = tk.Label(self, text=self.str_stasiun, bg='white')
        self.str_dewasa = "1 Dewasa x IDR " + str(master.harga)
        self.lbl_dewasa = tk.Label(self, text=self.str_dewasa, bg='white')
        self.lbl_jadwal = tk.Label(self, text=self.master.berangkattiba, bg='white')
        self.lbl_kelas = tk.Label(self, text=master.kelas, font=(self.font, 14, 'bold'), bg='white')
        self.str_harga = "IDR " + str(master.harga) + ",00"
        self.lbl_harga = tk.Label(self, text=self.str_harga, font=(self.font, 14, 'bold'), bg='white')
        self.lbl_subtotal = tk.Label(self, text="Subtotal", font=(self.font, 8), bg='white')

        self.add(self.lbl_nama, 50, 98)
        self.obj_stasiun = self.add(self.lbl_stasiun, 50, 165)
        self.add(self.lbl_dewasa, 50, 165 + self.get_height(self.obj_stasiun))
        self.obj_kelas = self.add(self.lbl_kelas, 300, 125)
        self.add(self.lbl_jadwal, 50, 125)
        self.x_harga = 300 + self.get_width(self.obj_kelas) + 30
        self.obj_harga = self.add(self.lbl_harga, self.x_harga, 98)
        self.add(self.lbl_subtotal, self.x_harga + self.get_width(self.obj_harga) / 3,
                98 + self.get_height(self.obj_harga))

        # label isi biodata
        self.bio_nama = tk.Label(self, text="Nama", font=(self.font, 9), bg='white')
        self.bio_email = tk.Label(self, text="Email", font=(self.font, 9), bg='white')
        self.bio_notelepon = tk.Label(self, text="No. Telepon", font=(self.font, 9), bg='white')
        self.bio_noktp = tk.Label(self, text="Nomor KTP", font=(self.font, 9), bg='white')

        self.entry_nama = tk.Entry(self, font=(self.font, 9), width=50)
        self.entry_email = tk.Entry(self, font=(self.font, 9), width=50)
        self.entry_notelepon = EntryDigit(self, font=(self.font, 9), width=50)
        self.entry_noktp = EntryDigit(self, font=(self.font, 9), width=50)

        self.obj_bio = self.add(self.bio_nama, 50, 300)
        self.y = self.get_height(self.obj_bio) + 10
        self.add(self.bio_email, 50, 300 + self.y)
        self.obj_telepon = self.add(self.bio_notelepon, 50, 300 + self.y * 2)
        self.add(self.bio_noktp, 50, 300 + self.y * 3)

        self.x = self.get_width(self.obj_telepon) + 80
        self.obj_entry = self.add(self.entry_nama, self.x, 300)
        self.add(self.entry_email, self.x, 300 + self.y)
        self.add(self.entry_notelepon, self.x, 300 + self.y * 2)
        self.add(self.entry_noktp, self.x, 300 + self.y * 3)

        # button pesan dan batal
        self.btn_pesan = tk.Button(self, text="Pesan", image=master.btn_pesan,
                                    command=self.pesan_sekarang)
        self.btn_batal = tk.Button(self, text="Batal", image=master.btn_batalpesan,
                                    command=lambda: master.switch_frame(PembelianTiket))

        self.pesan_obj = self.add(self.btn_pesan, 500, 440)
        self.add(self.btn_batal, 490 - self.get_width(self.pesan_obj), 440)

        # background image
        self.bg = self.canvas.create_image(
            0, 0, anchor=tk.NW, image=self.master.bg_pembayaran)

    def cancel_pembayaran(self):
        self.master.b_kereta = ""
        self.master.stasiun_awal = ""
        self.master.stasiun_tujuan = ""
        self.master.kelas = ""
        self.master.harga = 0
        self.master.berangkattiba = ""
        self.master.tanggal = ""

    # fungsi untuk menambahkan widget ke dalam canvas
    def add(self, widget, x, y):
        canvas_window = self.canvas.create_window(
            x, y, anchor=tk.NW, window=widget)
        return canvas_window

    def get_width(self, widget):
        bound = self.canvas.bbox(widget)
        return bound[2] - bound[0]

    def get_height(self, widget):
        bound = self.canvas.bbox(widget)
        return bound[3] - bound[1]

    def acak_kursi(self):
        angka = random.randint(1, 31)
        abjad = random.choice("ABCDEF")
        kursi = str(angka) + abjad

        tidak_tersedia = []
        conn = sqlite3.connect("db/Tiket.db")
        cursor = conn.execute("SELECT Kursi FROM TiketSaya WHERE StasiunAwal = '{}' AND StasiunTujuan = '{}'".
                                format(self.master.stasiun_awal, self.master.stasiun_tujuan))
        for row in cursor:
            tidak_tersedia.append(row[0])

        conn.close()

        # implementasi prolog list untuk mengetahui apakah kursi yang dipilih oleh sistem masih tersedia

        query = "kursi_sudah_ada('{}', {}, X)".format(kursi, tidak_tersedia)

        kursi_tidak_tersedia = 0
        for soln in self.master.prolog.query(query):
            kursi_tidak_tersedia = soln["X"]
            break

        if kursi_tidak_tersedia:
            conn.close()
            self.acak_kursi()

        return kursi


    def pesan_sekarang(self):
        if self.entry_nama.get() == "" or self.entry_email.get() == "" or self.entry_notelepon.get() == "" or self.entry_noktp.get() == "":
            messagebox.showerror("Gagal", "Tidak boleh ada field yang kosong")
            return

        email_pattern = re.compile(r"(^[a-zA-z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
        email_valid = email_pattern.findall(self.entry_email.get())

        if (email_valid == []):
            messagebox.showerror("Gagal", "Email tidak valid")
            return

        conn = sqlite3.connect("db/Tiket.db")

        cursor = conn.execute("SELECT nilai FROM Saldo")
        saldo = 0
        for row in cursor:
            saldo = row[0]

        # implementasi prolog aritmatika mengurangi saldo sekarang dengan harga tiket
        query = "bayar({}, {}, X)".format(saldo, self.master.harga)
        saldo_baru = self.master.eksekusi_prolog(query, "X")

        if(saldo_baru >= 0):
            cursor = conn.execute("UPDATE Saldo SET nilai = {}".format(saldo_baru))
        else:
            conn.close()
            messagebox.showwarning("Gagal", "Saldo tidak mencukupi, saldo anda: {}".format(saldo))
            return

        cursor = conn.execute("INSERT INTO TiketSaya (Nama, Email, NomorKTP, NomorTelepon, "
                            + "NamaKereta, StasiunAwal, StasiunTujuan, Kelas, Harga, Kursi, Jadwal, Tanggal) "
                            + "VALUES ('{}', '{}', {}, {}, '{}', '{}', '{}', '{}', {}, '{}', '{}', '{}')".format(
                                self.entry_nama.get(), self.entry_email.get(), self.entry_noktp.get(), self.entry_notelepon.get(),
                                self.master.b_kereta, self.master.stasiun_awal, self.master.stasiun_tujuan, self.master.kelas,
                                self.master.harga, self.acak_kursi(), self.master.berangkattiba, self.master.tanggal
                            ))

        conn.commit()
        conn.close()
        messagebox.showinfo("Sukses", "Pembelian tiket sukses, tiket telah masuk di tiket saya")
        self.master.switch_frame(PembelianTiket)
