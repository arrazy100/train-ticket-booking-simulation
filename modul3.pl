% predikat

slide(1, "HalamanUtama.png").
slide(2, "BeliTiket1.png").
slide(3, "BeliTiket2.png").
slide(4, "BeliTiket3.png").
slide(5, "BeliTiket4.png").
slide(6, "BeliTiket5.png").
slide(7, "BeliTiket6.png").
slide(8, "TiketSaya1.png").
slide(9, "TiketSaya2.png").
slide(10, "TiketSaya3.png").
slide(11, "TopUp1.png").
slide(12, "TopUp2.png").
slide(13, "TopUp3.png").

% aritmatika

bayar(SALDO, HARGA, OUTPUT) :-
    OUTPUT is SALDO - HARGA.

% logika

atau(A, B, C, D) :-
    A == B ; C == D.

validasi_input(A, B, C, D, OUTPUT) :-
    atau(A, B, C, D), OUTPUT = 1 ;
    OUTPUT = 0.

% list

kursi_sudah_ada(X, [X|_], 1).
kursi_sudah_ada(X, [_|T], OUTPUT) :-
    kursi_sudah_ada(X, T, OUTPUT) ; OUTPUT = 0.

% list of list

page_item([], _, []).
page_item(L, N, [H|T]) :-
    length(H, N),
    append(H, LTail, L),
    page_item(LTail, N, T) ;
    append(H, [], L),
    page_item([], N, T).

% binary tree

tree_sort(List, Output) :-
    construct(List, X),
    convert(X, Output).

add(X,nil,tree(X,nil,nil)).
add(X,tree(Root,L,R),tree(Root,L1,R)) :- X @=< Root, add(X,L,L1).
add(X,tree(Root,L,R),tree(Root,L,R1)) :- X @>= Root, add(X,R,R1).

construct(L,T) :- construct(L,T,nil).

construct([],T,T).
construct([N|Ns],T,T0) :- add(N,T0,T1), construct(Ns,T,T1).

convert(nil, []).
convert(tree(Element, Left, Right), Rest) :-
    convert(Left, LeftList),
    convert(Right, RightList),
    append(LeftList, [Element|RightList], Rest).