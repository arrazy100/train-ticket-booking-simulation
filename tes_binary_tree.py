from pyswip import Prolog

prolog = Prolog()
prolog.consult("modul3.pl")

lst = [3, 7, 5, 5, 3, 9]

q = "tree_sort({}, X)".format(lst)

for soln in prolog.query(q):
    print(soln["X"])