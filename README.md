<b>Sebelum menjalankan program, terlebih dulu install semua module yang dibutuhkan</b><br/>
<br/>
Install Python (32 bit / 64 bit)<br/>
<br/>
Install Swipl (32 bit / 64 bit)<br/>
<br/>
Versi bit python dan swipl harus sama<br/>
Jika python yang diinstall adalah versi 32 bit, make versi swipl yang diinstall juga harus versi 32 bit<br/>
<br/>
Buka CMD, kemudian ketikkan perintah dibawah ini:<br/>
<br/>
cd folder/path/to/projects<br/>
pip install -r requirements.txt atau pip3 install -r requirements.txt<br/>
<br/>
Run aplikasi:<br/>
python SistemTiket.py atau python3 SistemTiket.py<br/>
<br/>
<b>Implementasi Prolog</b><br/>
<br/>
Implementasi predikat ada pada file "Panduan.py" untuk mendapatkan daftar gambar yang akan ditampilkan pada slide panduan<br/>
<br/>
Implementasi list ada pada file "PembelianTiket.py" untuk mengetahui apakah kursi masih tersedia<br/>
<br/>
Implementasi aritmatika ada pada file "PembelianTiket.py" untuk mendapatkan nilai saldo setelah dikurangi harga tiket yang mau dibeli<br/>
<br/>
Implementasi logika ada pada file "HalamanUtama.py", jika memiliki tiket, maka menu TiketSaya dapat diakses<br/>
<br/>
Implementasi list of list ada pada file "PembelianTiket.py" untuk membagi daftar tiket yang ditampilkan menjadi beberapa halaman<br/>
<br/>
Implementasi binary tree ada pada file "TiketSaya.py" untuk mengurutkan koleksi daftar tiket dari data terkecil ke data terbesar<br/>