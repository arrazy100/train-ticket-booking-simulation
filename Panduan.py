import tkinter as tk

from PIL import Image, ImageTk

# pembuatan kelas Panduan

class Panduan(tk.Frame):

    # fungsi init atau constructor
    def __init__(self, master):
        tk.Frame.__init__(self, master)

        # background image
        self.canvas = tk.Canvas(self, width=640, height=480)
        self.canvas.pack()
        self.bg = self.canvas.create_image(
            0, 0, anchor=tk.NW, image=master.bg_panduan)

        self.canvas.create_rectangle(184, 89, 414+184, 311+89, outline='white')

        # foto panduan
        # implementasi prolog predikat
        self.cur_slide = 1
        self.jumlah_slide = 13
        self.panduan = []
        for i in range(1, self.jumlah_slide + 1):
            query = "slide({}, X)".format(i)
            image = ""
            for soln in master.prolog.query(query):
                image = soln["X"].decode("utf-8")

            image = "panduan/" + image

            pil = Image.open(
                str(image)).resize((413, 310), Image.ANTIALIAS)
            panduan = ImageTk.PhotoImage(pil)

            self.panduan.append(panduan)

        self.slide = self.canvas.create_image(
            185, 90, anchor=tk.NW, image=self.panduan[self.cur_slide - 1])

        self.btn_back_obj = self.canvas.create_image(
            148, 225, anchor=tk.NW, image=master.btn_back)
        self.btn_next_obj = self.canvas.create_image(
            605, 225, anchor=tk.NW, image=master.btn_next)

        self.canvas.tag_bind(
            self.btn_next_obj, "<Button-1>", lambda event: self.change_slide(self.cur_slide + 1))
        self.canvas.tag_bind(
            self.btn_back_obj, "<Button-1>", lambda event: self.change_slide(self.cur_slide - 1))

    def add(self, widget, x, y):
        canvas_window = self.canvas.create_window(
            x, y, anchor=tk.NW, window=widget)

    def change_slide(self, id):
        if (id >= 1) and (id <= self.jumlah_slide):
            self.cur_slide = id
            self.canvas.itemconfigure(self.slide, image=self.panduan[id - 1])
