import tkinter as tk

from tkinter import simpledialog

import sqlite3

#pembuatan kelas Jadwal Kereta
class TopUp(tk.Frame):

	#fungsi init atau constructor
	def __init__(self, master):
		tk.Frame.__init__(self, master)

		#background image
		self.canvas = tk.Canvas(self, width=640, height=480)
		self.canvas.pack()
		self.bg = self.canvas.create_image(0, 0, anchor=tk.NW, image=master.bg_topup)

		#membuat label saldo
		self.saldo = 0
		conn = sqlite3.connect("db/Tiket.db")
		cursor = conn.execute("SELECT nilai from Saldo")
		for row in cursor:
			self.saldo = row[0]
		conn.close()
		self.lbl_saldo = tk.Label(self, text=self.saldo, font=("Sans Serif", 16, 'bold'), bg='white')
		self.add(self.lbl_saldo, 220, 113)

		#membuat button back to home
		self.btn_home = tk.Button(self, image=master.btn_backtohome,
							command=lambda: master.switch_frame(master.HalamanUtama))
		self.add(self.btn_home, 161, 307)

		#membuat button top up
		self.btn_topup_obj = self.canvas.create_image(280, 187, anchor=tk.NW, image=master.btn_topup)
		self.canvas.tag_bind(self.btn_topup_obj, "<Button-1>", self.top_up_window)

	def add(self, widget, x, y):
		canvas_window = self.canvas.create_window(x, y, anchor=tk.NW, window=widget)

	def top_up_window(self, event):
		saldo = simpledialog.askinteger("Top Up", "Mau top up berapa?")

		cur_saldo = 0

		if saldo:
			conn = sqlite3.connect("db/Tiket.db")
			cursor = conn.execute("SELECT nilai FROM Saldo")
			for row in cursor:
				cur_saldo = row[0]

			saldo = saldo + cur_saldo

			if saldo >= 1000000:
				saldo = 1000000

			cursor = conn.execute("UPDATE Saldo SET nilai = {}".format(saldo))
			conn.commit()
			conn.close()
			self.lbl_saldo["text"] = saldo