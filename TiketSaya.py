import tkinter as tk

from tkinter import simpledialog, messagebox

import os
import math
import sqlite3

# pembuatan kelas TiketSaya


class TiketSaya(tk.Frame):

    # fungsi init atau constructor
    def __init__(self, master):
        tk.Frame.__init__(self, master)

        self.canvas = tk.Canvas(self, width=640, height=480)
        self.canvas.pack()
        self.bg = self.canvas.create_image(
            0, 0, anchor=tk.NW, image=self.master.bg_tiketsaya)

        self.font = "Helvetica Neue Light"
        if os.name == 'nt':
            self.font = "Sans Serif"

        # membuat button back to home
        self.btn_backtohome = tk.Button(self, image=self.master.btn_backtohome,
                                        command=lambda: self.master.switch_frame(self.master.HalamanUtama))
        self.add(self.btn_backtohome, 160, 410)

        # membuat button back dan next
        self.btn_back_obj = self.canvas.create_image(
            20, 214, anchor=tk.NW, image=self.master.btn_back)
        self.btn_next_obj = self.canvas.create_image(
            588, 214, anchor=tk.NW, image=self.master.btn_next)

        # membuat button untuk daftar tiket
        self.daftar_tiket = []
        self.cur_pages = 0

        conn = sqlite3.connect("db/Tiket.db")
        data = conn.execute("SELECT * FROM TiketSaya")

        d_harga = []

        for d in data:
            self.daftar_tiket.append(d)
            d_harga.append(d[9])

        conn.close()

        # implementasi prolog binary tree mengurutkan list dari data terkecil ke terbesar
        query = "tree_sort({}, X)".format(d_harga)
        new_d_harga = self.master.eksekusi_prolog(query, "X")

        for i in range(len(self.daftar_tiket)):
            index = new_d_harga.index(self.daftar_tiket[i][9])
            temp = self.daftar_tiket[i]
            self.daftar_tiket[i] = self.daftar_tiket[index]
            self.daftar_tiket[index] = temp

        self.canvas_list = []
        if self.daftar_tiket != []:
            self.show_list(self.daftar_tiket, 1, 4)

        if (len(self.daftar_tiket) > 4):
            self.canvas.tag_bind(self.btn_next_obj, "<Button-1>", self.klik_next)

    def add(self, widget, x, y):
        canvas_window = self.canvas.create_window(
            x, y, anchor=tk.NW, window=widget)

        return canvas_window

    def add_in_frame(self, canvas, widget, x, y):
        canvas_window = canvas.create_window(
            x, y, anchor=tk.NW, window=widget)

        return canvas_window

    def klik_next(self, event):
        if (self.cur_pages == self.count_pages(self.daftar_tiket, 4)):
            self.canvas.tag_unbind(self.btn_next_obj, "<Button-1>")
            return

        for canvas in self.canvas_list:
            canvas.pack()
            canvas.pack_forget()
            self.canvas_list = []

        self.canvas.tag_bind(self.btn_back_obj, "<Button-1>", self.klik_back)
        self.show_list(self.daftar_tiket, self.cur_pages + 1, 4)

    def klik_back(self, event):
        if (self.cur_pages == 1):
            self.canvas.tag_unbind(self.btn_back_obj, "<Button-1>")
            self.canvas.tag_bind(self.btn_next_obj, "<Button-1>", self.klik_next)
            return

        for canvas in self.canvas_list:
            canvas.pack()
            canvas.pack_forget()
            self.canvas_list = []

        self.show_list(self.daftar_tiket, self.cur_pages - 1, 4)

    def count_pages(self, lst, number_per_pages):

        page_item = [lst[i:i+number_per_pages]
                     for i in range(0, len(lst), number_per_pages)]
        c = 0

        for i in range(len(page_item)):
            c += 1

        return c

    def show_list(self, lst, pages, number_per_pages):

        if self.cur_pages == pages:
            return

        # memecah list menjadi list of list dengan setiap element list of list berdasarkan jumlah halaman yang telah ditentukan
        page_item = [lst[i:i+number_per_pages]
                     for i in range(0, len(lst), number_per_pages)]

        # perulangan untuk memunculkan semua daftar tiket
        for i in range(len(page_item[pages - 1])):
            canvas = tk.Canvas(self, width=251, height=122)
            bg = canvas.create_image(
                0, 0, anchor=tk.NW, image=self.master.bg_lbltiket)
            canvas.pack()

            cur = page_item[pages - 1][i]

            # label tiket bagian atas
            str_nama = cur[5]
            lbl_nama = tk.Label(self, text=str_nama, font=(self.font, 8, 'bold'), bg='white')
            str_stasiun = cur[6] + " - " + cur[7]
            lbl_stasiun = tk.Label(self, text=str_stasiun, font=(self.font, 7, 'bold'), bg='white')
            lbl_kelas = tk.Label(self, text=cur[8], font=(
                self.font, 7, 'bold'), bg='white')
            str_harga = "IDR " + str(cur[9]) + ",00"
            lbl_harga = tk.Label(self, text=str_harga, font=(
                self.font, 7, 'bold'), bg='white')
            lbl_pemilik = tk.Label(self, text="Pemilik: " + cur[1], font=(self.font, 8), bg='white')
            lbl_kursi = tk.Label(self, text=cur[10], font=(self.font, 8, 'bold'), bg='white')
            lbl_berangkattiba = tk.Label(self, text=cur[11], font=(self.font, 8), bg='white')
            lbl_tanggal = tk.Label(self, text=cur[12], font=(self.font, 8), bg='white')

            self.add_in_frame(canvas, lbl_nama, 10, 10)
            self.add_in_frame(canvas, lbl_stasiun, 10, 30)
            self.add_in_frame(canvas, lbl_kelas, 10, 50)
            self.add_in_frame(canvas, lbl_harga, 10, 70)
            self.add_in_frame(canvas, lbl_pemilik, 10, 95)
            self.add_in_frame(canvas, lbl_kursi, 210, 95)
            self.add_in_frame(canvas, lbl_tanggal, 190, 70)
            self.add_in_frame(canvas, lbl_berangkattiba, 140, 50)

            canvas.tag_bind(bg, "<Button-1>", lambda event, i=i: self.batalkan(i))

            self.add(canvas, 65 + (260 * (i % 2)), 98 + (152 * math.floor(i / 2)))
            self.canvas_list.append(canvas)

        self.cur_pages = pages

    def get_width(self, widget):
        bound = self.canvas.bbox(widget)
        return bound[2] - bound[0]

    def get_height(self, widget):
        bound = self.canvas.bbox(widget)
        return bound[3] - bound[1]

    def batalkan(self, id):
        t = self.daftar_tiket[id][0]
        yakin = messagebox.askyesno("Perhatian", "Yakin ingin membatalkan tiket?")

        conn = sqlite3.connect("db/Tiket.db")

        if yakin:
            cursor = conn.execute("DELETE FROM TiketSaya WHERE ID = {}".format(t))
            conn.commit()

        cursor = conn.execute("SELECT id FROM TiketSaya")
        tiket = False
        for row in cursor:
            tiket = True
            break
        conn.close()

        if tiket and yakin:
            self.master.switch_frame(TiketSaya)
        elif not tiket:
            self.master.switch_frame(self.master.HalamanUtama)
