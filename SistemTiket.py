from pyswip import Prolog, Query

import tkinter as tk
from tkinter import messagebox
from tkinter import ttk

from PIL import Image, ImageTk

import sqlite3

from collections import defaultdict

from HalamanUtama import *

# pembuatan kelas utama, yaitu kelas SistemTiket
class SistemTiket(tk.Tk):

    # fungsi init atau constructor
    def __init__(self):
        tk.Tk.__init__(self)
        self.title("Simulasi Sistem Tiket Kereta Api")  # mengubah judul window
        self.geometry('640x480')
        self.resizable(0, 0)
        self._frame = None  # frame sekarang masih kosong

        # load resources
        self.load_resources()
        self.HalamanUtama = HalamanUtama
        self.prolog = Prolog()
        self.prolog.consult("modul3.pl")

        # data pembelian
        self.b_kereta = ""
        self.stasiun_awal = ""
        self.stasiun_tujuan = ""
        self.kelas = ""
        self.harga = 0
        self.berangkattiba = ""
        self.tanggal = ""

        # ubah frame menjadi frame HalamanUtama
        self.switch_frame(self.HalamanUtama)

    # fungsi untuk load semua resources seperti logo, gambar background, dll
    # fungsi ini digunakan untuk mempercepat perpindahan antar frame agar tidak mengulangi pembuatan objek resources
    # pembuatan objek resources hanya terjadi satu kali, yaitu pada kelas window
    def load_resources(self):

        # database
        conn = sqlite3.connect("db/Tiket.db")

        cursor = conn.execute("SELECT * FROM kereta")

        self.nama_kereta = []
        self.awal = []
        self.tujuan = []
        self.jarak = []
        self._kelas = []
        self.berangkat = []
        self.tiba = []
        for row in cursor:
            self.nama_kereta.append(row[0])
            self.awal.append(row[1])
            self.tujuan.append(row[2])
            self.jarak.append(row[3])
            self._kelas.append(row[4])
            self.berangkat.append(row[5])
            self.tiba.append(row[6])

        conn.close()

        # tes dict
        self.k_a = defaultdict(list)
        self.a_t = defaultdict(list)

        for key, value in zip(self.nama_kereta, self.awal):
            self.k_a[key].append(value)

        for key, value in zip(self.awal, self.tujuan):
            self.a_t[key].append(value)

        # algoritma mendapatkan list stasiun keberangkatan
        self.s_awal = list(self.a_t.keys())

        # variabel resources
        self.pil_bg = Image.open(
            "bg/bg_image.jpg").resize((640, 480), Image.ANTIALIAS)
        self.img_bg = ImageTk.PhotoImage(self.pil_bg)

        self.pil_bg_pembayaran = Image.open(
            "bg/bg_pembayaran.png").resize((640, 480), Image.ANTIALIAS)
        self.bg_pembayaran = ImageTk.PhotoImage(self.pil_bg_pembayaran)

        self.pil_bg_beli_tiket = Image.open(
            "bg/bg_back.png").resize((640, 480), Image.ANTIALIAS)
        self.bg_beli_tiket = ImageTk.PhotoImage(self.pil_bg_beli_tiket)

        # load gambar train-ticket.png kemudian di resize menjadi 30x30 dengan mode antialias
        self.pil_beli_tiket = Image.open(
            "logo/train-ticket.png").resize((30, 30), Image.ANTIALIAS)
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.img_beli_tiket = ImageTk.PhotoImage(self.pil_beli_tiket)

        self.pil_lbltiket = Image.open(
            "bg/bg_lbltiket.png")
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.bg_lbltiket = ImageTk.PhotoImage(self.pil_lbltiket)

        # load gambar my-ticket.png kemudian di resize menjadi 30x30 dengan mode antialias
        self.pil_tiket_saya = Image.open(
            "logo/my-ticket.png").resize((30, 30), Image.ANTIALIAS)
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.img_tiket_saya = ImageTk.PhotoImage(self.pil_tiket_saya)

        # load gambar schedule.png kemudian di resize menjadi 30x30 dengan mode antialias
        self.pil_jadwal = Image.open(
            "logo/schedule.png").resize((30, 30), Image.ANTIALIAS)
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.img_jadwal = ImageTk.PhotoImage(self.pil_jadwal)

        # load gambar cancel.png kemudian di resize menjadi 30x30 dengan mode antialias
        self.pil_batalkan = Image.open(
            "logo/cancel.png").resize((30, 30), Image.ANTIALIAS)
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.img_batalkan = ImageTk.PhotoImage(self.pil_batalkan)

        # load gambar rise.png kemudian di resize menjadi 30x30 dengan mode antialias
        self.pil_prediksi_harga = Image.open(
            "logo/rise.png").resize((30, 30), Image.ANTIALIAS)
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.img_prediksi_harga = ImageTk.PhotoImage(self.pil_prediksi_harga)

        # load gambar help.png kemudian di resize menjadi 30x30 dengan mode antialias
        self.pil_panduan = Image.open(
            "logo/help.png").resize((30, 30), Image.ANTIALIAS)
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.img_panduan = ImageTk.PhotoImage(self.pil_panduan)

        self.pil_bg_tiketsaya = Image.open(
            "bg/bg_tiketsaya.png").resize((640, 480), Image.ANTIALIAS)
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.bg_tiketsaya = ImageTk.PhotoImage(self.pil_bg_tiketsaya)

        self.pil_btn_backtohome = Image.open(
            "bg/btn_backtohome.png")
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.btn_backtohome = ImageTk.PhotoImage(self.pil_btn_backtohome)

        self.pil_btn_next = Image.open(
            "bg/next.png")
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.btn_next = ImageTk.PhotoImage(self.pil_btn_next)

        self.pil_btn_back = Image.open(
            "bg/back.png")
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.btn_back = ImageTk.PhotoImage(self.pil_btn_back)

        self.pil_bg_batalkantiket = Image.open(
            "bg/bg_batalkantiket.png").resize((640, 480), Image.ANTIALIAS)
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.bg_batalkantiket = ImageTk.PhotoImage(self.pil_bg_batalkantiket)

        self.pil_btn_batal = Image.open(
            "bg/btn_batal.png")
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.bg_btn_batal = ImageTk.PhotoImage(self.pil_btn_batal)

        self.pil_btn_pesan = Image.open(
            "bg/btn_pesan.png")
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.btn_pesan = ImageTk.PhotoImage(self.pil_btn_pesan)

        self.pil_btn_batalpesan = Image.open(
            "bg/btn_batalpesan.png")
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.btn_batalpesan = ImageTk.PhotoImage(self.pil_btn_batalpesan)

        self.pil_bg_topup = Image.open(
            "bg/bg_topup.png")
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.bg_topup = ImageTk.PhotoImage(self.pil_bg_topup)

        self.pil_btn_topup = Image.open(
            "bg/btn_topup.png")
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.btn_topup = ImageTk.PhotoImage(self.pil_btn_topup)

        self.pil_bg_panduan = Image.open(
            "bg/bg_panduan.png").resize((640, 480), Image.ANTIALIAS)
        # mengatur objek gambar agar bisa di load ke dalam GUI tkinter
        self.bg_panduan = ImageTk.PhotoImage(self.pil_bg_panduan)

        self.c_style = ttk.Style()
        self.c_style.theme_create('combostyle', parent='alt', settings={'TCombobox': {
            'Configure': {
                'fieldbackground': 'white',
                'background': 'white'
            }
        }
        })
        self.c_style.theme_use('combostyle')

    # fungsi untuk mengubah frame

    def switch_frame(self, frame_class):
        new_frame = frame_class(self)  # mengambil frame dari frame_class

        # jika frame sekarang tidak kosong
        if self._frame is not None:
            self._frame.destroy()  # hapus frame sekarang

        # mengubah frame sekarang menjadi frame baru
        self._frame = new_frame
        self._frame.grid(row=0, column=0)

    def add(self, widget, x, y):
        canvas_window = self.canvas.create_window(
            x, y, anchor=tk.NW, window=widget)

    def eksekusi_prolog(self, query, output_var):
        output = []
        for soln in self.prolog.query(query, maxresult=1):
            output.append(soln[output_var])

        return output[0]


# fungsi main
if __name__ == "__main__":
    app = SistemTiket()
    app.mainloop()
